import { all, fork } from "redux-saga/effects";
import Employee from "./EmployeeSaga";

export default function* rootSaga() {
  yield all([...Employee,]);
}