import { combineReducers } from 'redux';

import Employee from './EmployeeReducer';

export default combineReducers({
    Employee,
});
